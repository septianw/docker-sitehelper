#!/bin/bash

rmdc () {
    docker ps -a | grep $1 | awk '{print $1}' | xargs --no-run-if-empty docker rm
}

rmdi () {
    docker images | grep $1 | awk '{print $3}' | xargs --no-run-if-empty docker rmi
}

docker-compose stop

umount code space
rmdir code space
rm *.img
rmdc $1
rmdi $1
rm .lock
